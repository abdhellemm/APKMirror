package taco.apkmirror.fragments

import android.os.Build
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import de.psdev.licensesdialog.LicensesDialog
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20
import de.psdev.licensesdialog.licenses.MITLicense
import de.psdev.licensesdialog.model.Notice
import de.psdev.licensesdialog.model.Notices
import taco.apkmirror.R
import taco.apkmirror.util.startIntent

class PreferencesFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)

        val colorNavBar = findPreference<Preference>("color_navigation_bar")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            colorNavBar?.isVisible = true
        }

        val sourceCode = findPreference<Preference>("source_code")
        val libs = findPreference<Preference>("libs")
        val xda = findPreference<Preference>("xda")

        sourceCode?.startPreferenceIntent("https://gitlab.com/TacoTheDank/APKMirror")

        libs?.setOnPreferenceClickListener {
            val n = Notices().apply {
                addNotice(
                    Notice(
                        "jsoup: Java HTML Parser",
                        "https://github.com/jhy/jsoup",
                        "Copyright (c) 2009-2020 Jonathan Hedley <https://jsoup.org/>",
                        MITLicense()
                    )
                )
                addNotice(
                    Notice(
                        "Android-AdvancedWebView (forked from delight-im/Android-AdvancedWebView)",
                        "https://github.com/TacoTheDank/Android-AdvancedWebView",
                        "Copyright (c) delight.im (https://www.delight.im/)",
                        MITLicense()
                    )
                )
                addNotice(
                    Notice(
                        "Material Dialogs",
                        "https://github.com/afollestad/material-dialogs",
                        "Copyright (c) 2018 Aidan Follestad",
                        MITLicense()
                    )
                )
                addNotice(
                    Notice(
                        "LicensesDialog",
                        "https://github.com/PSDev/LicensesDialog",
                        "Copyright 2013 Philip Schiffer",
                        ApacheSoftwareLicense20()
                    )
                )
            }
            LicensesDialog.Builder(requireActivity())
                .setNotices(n)
                .setTitle(R.string.libraries)
                .build()
                .show()
            true
        }

        xda?.startPreferenceIntent(
            "https://forum.xda-developers.com/android/apps-games/apkmirror-web-app-t3450564"
        )
    }

    private fun Preference.startPreferenceIntent(uri: String) {
        setOnPreferenceClickListener {
            activity?.startIntent(uri)
            true
        }
    }
}
